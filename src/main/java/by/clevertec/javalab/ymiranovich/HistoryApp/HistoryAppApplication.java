package by.clevertec.javalab.ymiranovich.HistoryApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HistoryAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(HistoryAppApplication.class, args);
    }
}
